var selected_promo =
{
    days : 3,
    nights : 2
}

var cached_tours = [];
var send_dialog;
var uuid;
var uuidListener;

var minPax = -1;
var rooms = 0;

/* var templateID = "template_wae1veu"; */
var templateID = "template_qmd235q";
var service_id = "service_vegtgqv";
var myRegion = 'all';
var regionResultCount = 4;

var currentTab = 0; // Current tab is set to be the first tab (0)
var tabTitles = ["Select Destination", "Quotation Form", "Verify Email", "Review Quotation"];

var emailIsVerified = false;
var emailSent = false;
var booked = false;
 // Display the current tab

$(document).ready( function () {

    emailjs.init("user_be1AAhAR49gurd1HFC44T");

    addButtonsCallback();

    getUUID();    

    checkIfVerifyWithLink();
        
});

function attachUUIDListeners()
{
    if(uuidListener != null)
        uuidListener();
    uuidListener = db.collection("constants")
    .onSnapshot(function(snapshot) {
        
        snapshot.docChanges().forEach(function(change) {
            if(change.doc.id == uuid)
            {
                getUUID();
            }
        });

    }, function(error) {
        
    });
}

function getUUID()
{
    uuid = create_UUID();

    db.collection("constants").doc(uuid).get().then(function(doc) {
        if (doc.exists) {
            console.log("Document data:", doc.data());
            getUUID();
        } else {
            // doc.data() will be undefined in this case
            attachUUIDListeners();
            console.log("No such document!");
            fetchDatabaseTourList();
            $("#dotted").html("Request ID : " + uuid);
        }
    }).catch(function(error) {
        console.log("Error getting document:", error);
    });
}

function fetchDatabaseTourList()
{
    $("#carousel_items_container").empty();

    regionResultCount = 0;
    var active_text = 'active';
    db.collection("Destinations").get().then(function(querySnapshot) {
        var size = querySnapshot.size;
        var z = 0;
        querySnapshot.forEach(function(doc) {
            // doc.data() is never undefined for query doc snapshots
            
            let data = doc.data();

            db.collection("Destinations").doc(doc.id).collection("tours")
            .get().then(function(querySnapshot2){
                querySnapshot2.forEach(function(tour){

                    let tour_data = tour.data();
                    let thisRegion = data['region'];
                    let thisCountry = data['country'];

                    cached_tours[tour.id] = tour_data;
                    cached_tours[tour.id]["region"] = thisRegion;
                    cached_tours[tour.id]["country"] = thisCountry;
                    cached_tours[tour.id]["key"] = tour.id;                    
                    
                    if(thisRegion == myRegion || myRegion == 'all' || (myRegion == "philippines" && thisCountry == "philippines"))
                    {


                        let newEntry = 
                        `<div class="carousel-item `+active_text+`">
                            <div class="col-md-3 tour_entry" data-region="`+data['region']+`" data-country="`+data['country']+`" data-key="`+tour.id+`">
                                <div class="card">
                                    <div class="card-img">
                                        <img src="`+ tour_data['image'] +`" class="img-fluid" alt="" referrerpolicy="no-referrer">
                                        <div class="price-tag"><p class="price-details">`+tour_data['name']+`<br>`+getFormatedMoney(tour_data['price'])+`<br>`+tour_data['days']+`D`+tour_data['nights']+`N</p></div>
                                    </div>
                                </div>
                            </div>
                        </div>`;

                        $("#carousel_items_container").append(newEntry);


                        if(active_text.length > 0)
                            active_text = '';

                        regionResultCount++;

                    }

                    //set selected_promo
                    var url_string = window.location.href
                    var url = new URL(url_string);
                    var code = url.searchParams.get("oobCode");

                    if(code == null)
                        return;
                    
                    let selectedPromoKey = url.searchParams.get("selectedPromoKey");
                    console.log("selectedPromoKey=" + selectedPromoKey);
                    if(selectedPromoKey in cached_tours){
                        selected_promo = cached_tours[selectedPromoKey];
                        clickedPromo($('.tour_entry[data-key='+selectedPromoKey+']'));                        
                    }

                });

                z++;
                if(z >= size)
                {
                    initCarousel();
                }
            });
        });

        // initCarousel();
    });
}

function reInitCarousel(){
    $("#carousel_items_container").empty();
    var active_text = 'active';
    regionResultCount = 0;


    for (const [key, value] of Object.entries(cached_tours)) {
        
        let thisRegion = value['region'];
        let thisCountry = value['country'];
        
        if(thisRegion == myRegion || myRegion == 'all' || (myRegion == "philippines" && thisCountry == "philippines")){

            let newEntry = 
            `<div class="carousel-item `+active_text+`">
                <div class="col-md-3 tour_entry" data-region="`+value['region']+`" data-country="`+value['country']+`" data-key="`+key+`">
                    <div class="card">
                        <div class="card-img">
                            <img src="`+ value['image'] +`" class="img-fluid" alt="" referrerpolicy="no-referrer">
                            <div class="price-tag"><p class="price-details">`+value['name']+`<br>`+getFormatedMoney(value['price'])+`<br>`+value['days']+`D`+value['nights']+`N</p></div>
                        </div>
                    </div>
                </div>
            </div>`;

            $("#carousel_items_container").append(newEntry);


            if(active_text.length > 0)
                active_text = '';

            regionResultCount++;
        }
    }
    initCarousel();
}


function clickedAdditional(elem)
{
    let index = parseInt($(elem).attr('data-index'));

    if (elem.checked == false){
        $("#table_body").find(".additional[data-index='"+index+"']").remove();
    } else {
        let additional = selected_promo.additionals[index];
        let splitted = additional.split("_");
        let price = parseInt(splitted[1]);
        
        let additional_to_add = 
        `<tr class="additional" data-price="`+price+`" data-index="`+index+`">
            <td><b>ADDITIONAL TOURS</b><br>`+splitted[0]+`</td>
            <td class="pax"></td>
            <td class="base_price">`+getFormatedMoney(price)+`</td>
            <td class="subtotals" data-value="0"></td>
        </tr>`;

        $("#table_body").append(additional_to_add);
    }
    
    updateFormData();
    
}

function clickedPromo(elem)
{
    console.log(elem);
    let key = elem.attr('data-key');
    
    let table_body = $("#table_body").get(0);
    while (table_body.lastChild.id !== 'table_row1') {
        table_body.removeChild(table_body.lastChild);
    }

    selected_promo = cached_tours[key];

    updateDates();
    // console.log(selected_promo);

    if('min_pax' in selected_promo)
        minPax = selected_promo.min_pax;

    $("#additional_container").empty();

    if('additionals' in selected_promo)
    {
        console.log("has additional");
        

        for(var i=0; i < selected_promo.additionals.length; i++)
        {
            let splitted = selected_promo.additionals[i].split("_");
            let name = splitted[0];
            let price = getFormatedMoney(parseInt(splitted[1]));
            let image_link = "";
            if(splitted.length > 1){
                for (let index = 2; index < splitted.length; index++) {
                    image_link += splitted[index] + ((index < splitted.length-1)? "_": "");
                }                
            }
           
            let newAdditional = `<div class="row">
			<div class="col-1 d-none d-sm-block">
					
                <a href="javascript:void(0)" class="info_buttons badge badge-pill badge-primary" data-img="`+image_link+`" data-title="`+name+`">?</a>		    
                </div>
                <div class="col-lg-6 col-xs-6 col-sm-6 additional_labels">                  
				
                        <input type="checkbox" class="form-check-input additional" id="optional`+i+`" onclick="clickedAdditional(this)" data-index="`+i+`">
                        
						<label class="form-check-label optional_check_boxes" for="optional`+i+`">`+name+`</label>
                   
                </div>

                <div class="col-lg-5 col-xs-6 col-sm-6">
                    <p>`+price+`</p>
                </div>
            </div>`;

            

            $("#additional_container").append(newAdditional);
        }
    }
    else
    {
        $("#table_body").find('.additional').remove();
    }

    let newTableRow = 
        `<tr class="additional" data-price="`+selected_promo.price+`">
            <td>`+selected_promo.description+`</td>
            <td class="pax"></td>
            <td class="base_price">`+getFormatedMoney(selected_promo.price)+`</td>
            <td class="subtotals" data-value="0"></td>
        </tr>`;

    $("#table_body").append(newTableRow);

    updateFormData();
    updatePax();
}

function initCarousel()
{
    let items = document.querySelectorAll('.carousel .carousel-item')

    items.forEach((el) => {
        const minPerSlide = Math.min(4, regionResultCount);
        let next = el.nextElementSibling
        for (var i=1; i<minPerSlide; i++) {
            if (!next) {
                // wrap carousel by using first child
                next = items[0]
            }
            let cloneChild = next.cloneNode(true)
            el.appendChild(cloneChild.children[0])
            next = next.nextElementSibling
        }
    });    
}


function goSubmit(e)
{
    send_dialog = bootbox.dialog({
        message: '<p class="text-center mb-0"><i class="fa fa-spin fa-cog"></i> Sending your request...</p>',
        closeButton: false
    });

    // Add a new document with a generated id.
    db.collection("constants").doc(uuid).set({
        date: firebase.firestore.FieldValue.serverTimestamp()
    })
    .then(function() {




        html2canvas(document.getElementById("quote_summary_container"), {scale:0.5}).then(function(canvas) {
            //document.body.appendChild(canvas)


            var base64 = canvas.toDataURL("image/jpeg", 0.69);
            /*var imgElement = 
            `
                <!DOCTYPE html>
                <html lang="en">
                <head>
                <body>
                <img src="` + base64 + `">
                </body>
                 </html>
            `;*/

            //document.body.appendChild(canvas);

            //var imgElement = `<img src="cid:canvas" alt="My Impression">`

            $("#preferred_hotel_parent").hide();
            /*$("#preferred_hotel_label").show();
            $("#preferred_hotel_text").text($("#preferred_hotel").val());*/

            var imgElement = $("#quote_summary_container").html();

            console.log();

            let templateParams = 
            {
                to_name : "nbtalon07@gmail.com",
                from_name : $("#input_first_name").val() + " " + $("#input_last_name").val(),
                //message : $("#quote_summary_container").html(),
                message : imgElement

            }

            console.log(templateParams);

            emailjs.send(service_id, templateID, templateParams)
            .then(function(response) {
                
                let templateParamsForClient = 
                {
                    to_name: $('#input_email').val(),
                    from_name : "nbtalon07@gmail.com",
                    message : imgElement
                    //message : $("#quote_summary_container").html()
                }
                
                console.log(templateParamsForClient);
                
                emailjs.send(service_id, templateID, templateParamsForClient)
                .then(function(response2) {
                     var dialog = bootbox.dialog({
                        title: 'SUCCESS!',
                        message: "Your request has been sent successfully. Please wait for our response.",
                        size: 'large',
                        closeButton: false,
                        buttons: {
                            ok: {
                                label: "Ok",
                                className: 'btn-info',
                                callback: function(){
                                    window.location.replace("https://webook-c003e.web.app/create.html"); 
                                }
                            }
                        }
                    });

                    // do something in the background
                    send_dialog.modal('hide');

                }, function(error) {
                    bootbox.alert("Failed... \n" + error);
                    console.log('FAILED...', error);
                });
                

            }, function(error) {
                bootbox.alert("Failed... \n" + error);
                console.log('FAILED...', error);
            });

        });        

    })
    .catch(function(error) {
        bootbox.alert("Failed... \n" + error.message);
        console.error("Error writing document: ", error);
    });  
}

function updateTourVisibility()
{
    $(".tour_entry").each(function(){
        let thisRegion = $(this).attr('data-region');
        let thisCountry = $(this).attr('data-country');
        
        if(thisRegion == myRegion || myRegion == 'all' || (myRegion == "philippines" && thisCountry == "philippines"))
        {
            $(this).css('display', 'block');
        }
        else
        {
            $(this).css('display', 'none');
        }
    });

    console.log("done updateTourVisibility");
}

function addButtonsCallback()
{
    $("#additional_container").on('click', '.info_buttons', function(){
        let url = $(this).attr('data-img');
        let title = $(this).attr('data-title');
        bootbox.alert({
            scrollable:true,
            size: "large",
            title: title,
            message:`<img class="preview_tour_additional" src="` + url + `">`
        });
    });

    /*$("#book_button").on('click', function(e){
        if($("#mainForm").get(0).reportValidity())
          goSubmit(e);
      });*/


    $("#carousel_items_container").on('click', '.tour_entry', function(){
        clickedPromo($(this));
        showHighlight();
    });


    $( "form" ).each(function( index ) {
        this.reset();
    });

    /*$("#submitButton").on('click', function(e){
    if($("#editAccountForm").get(0).reportValidity())
        goSubmit(e);
    });*/

    let now = moment();
    let today = now.toDate();

    let endPromoDate = now.add(3, 'days').toDate();

    $('#input_travel_date_from').datepicker({
        autoclose:true,
        // minDate: moment().toDate(),
        startDate: "today",
        
        disableTouchKeyboard: true
    }).on('changeDate', function (ev) {
        console.log("date changed!");

        updateDates();

        updateFormData();
    });

    $('#input_travel_date_to').datepicker({
        autoclose:true,
        // minDate: moment().toDate(),
        startDate: endPromoDate,
        //setDate: moment().toDate(),
        disableTouchKeyboard: true
    })
    .on('changeDate', function (ev) {
        console.log("date changed!")
        updateFormData();
    });

    $("#input_nationality").on('change', function(e){
          nationalityChanged();
    });
    // $("#input_rooms").on('keyup', function(){
    //     console.log("changed!");
    //     updateRoom();
    // });

    if(emailIsVerified == false){
        $('#input_travel_date_from').datepicker("setDate", today);
        $('#input_travel_date_to').datepicker("setDate", endPromoDate);
    }    
    
    updateFormData();

    $(".filter_region").on('click', function(){ 
        clicked = true;       
        $(".filter_region").removeClass("active");
        $(this).addClass("active");

        myRegion = $(this).text().toLowerCase();

        //updateTourVisibility();    
        reInitCarousel();    
    });

    addPastePrevent($("form input"));

    $("#input_first_name, #input_last_name").on('change keydown paste input', function(){
        nameChanged();
    });

    $("#input_email").on('change keydown paste input', function(){
        emailChanged();
    });

    
    $("#input_address").on('change keydown paste input', function(){
        updateAddress();
    });
    
    $("#input_phone").on('change keydown paste input', function(){
        updatePhone();
    });

    $("#input_adults, #input_children, #input_infant").on('change keydown paste input', function(){
        updatePax();
        
    });

    /*$("#input_adults").on('change', function(){
        $(this).attr('readonly',true);
    });*/

    $("#preferred_hotel").on('change keydown paste input', function(){
        updateHotelName();
    });

    $("#date_issued").text(moment().format('DD MMMM YYYY'));
}

function updateAddress(){
    $("#t_address").html('<b>ADDRESS</b> ' + $("#input_address").val());
}

function updatePhone(){
    $("#t_contact").html('<b>CONTACT</b> ' + $("#input_phone").val());
}

function updateDates()
{
    //var startdate = new Date($('#input_travel_date_from').val());
    
    //var new_date = moment(startdate, "MM-DD-YYYY").add(selected_promo.days, 'days').toDate();
    //$('#input_travel_date_to').datepicker("setDate", new_date);
    //$('#input_travel_date_to').val(new_date);
}

function updateHotelName()
{
    let origText = $("#travel_date").html();
    let splitted = origText.split('<br>');

    $("#travel_date").html(splitted[0] + '<br>' + splitted[1] + '<br>' + '<p>Preferred Hotel : ' +  $("#preferred_hotel").val() + '</p>');
    //+"<br>Rooms:"+rooms);
}


function computeSubTotal()
{
    let total = 0;
    $(".subtotals").each(function(){
        let thisNum = 0;
        if(!isNaN(parseInt($(this).attr("data-value"))))
        {
            thisNum = parseInt($(this).attr("data-value"));
            total += thisNum;            
        }
    });

    let peso = getFormatedMoney(total);
    $("#subtotal_value").html('<b>'+peso+'</b>');

    $("#initial_quotation_price").html('<b>'+peso+'</b>');

    let vat = (total / 100) * 5;
    console.log("vat = " + vat);
    $("#subtotal_vat").text(accounting.formatMoney(vat, "₱", 2, ",", "."));


    $("#grand_total").html('<b>' + getFormatedMoney(total + vat) + '</b>');
    
}

function nationalityChanged(){
    let nationality = $("#input_nationality").val();
    $("#t_nationality").html('<b>NATIONALITY</b> ' + toTitleCase(nationality));
}

function emailChanged()
{
    let email = $("#input_email").val();
    $("#t_email").html('<b>EMAIL ADD.</b> ' + email)
}

function nameChanged()
{
    //Client name
    let first_name = $("#input_first_name").val();
    let last_name = $("#input_last_name").val();

    $(".buyer").html("<sup>To</sup>" + first_name + " " + last_name );
}



function updateFormData()
{
    let fromDate = moment($("#input_travel_date_from").val(), "MM/DD/YYYY").format('YYYY-MMMM-DD');
    let splittedFrom = fromDate.split("-");
    let day1 = splittedFrom[2];

    let toDate = moment($("#input_travel_date_to").val(), "MM/DD/YYYY").format('YYYY-MMMM-DD');
    let splittedTo = toDate.split("-");
    let day2 = splittedTo[2];

    let pax = updatePax();
 
    $("#travel_date").html('Travel Date: ' + splittedFrom[1] + ' ' + day1 + ' - ' + splittedTo[1] +' ' + day2 + 
    ' <br> Adult: '+pax[0]+' Children: ' + pax[1] + ' Infant: ' + pax[2] +
    '<br> ' + $("#preferred_hotel").val());
    //+"<br>Rooms:"+rooms);

    if(("hotel" in selected_promo))
    {
        let splitted = selected_promo.hotel.split("_");
        let hotel_name = splitted[0];
        let hotel_link = "";
        for (let index = 1; index < splitted.length; index++) {
            hotel_link += splitted[index] + ((index < splitted.length-1)? "_": "");
        }

        $("#partner_title").html(`Our partner hotel to accomodate you is <span class="underlined"><a target="_blank" href="`+hotel_link+`">`+hotel_name+`</a></span>`);
    }
    else
    {
        $("#partner_title").html("");
    }
}

function updateRoom()
{
    return;
    //this will add rooms according to min pax
    let room_count = 0;
    let temp_room = parseInt($("#input_rooms").val());
    let pax1 = 0;
    let tempPax1 = parseInt($("#input_adults").val());

    if(!isNaN(tempPax1))
        pax1 = tempPax1;

    if(!isNaN(temp_room))
        room_count = temp_room;

    let sum_room = 0;
    let origInput = 0;
    let tempOrigInput = $("#input_rooms").val();
    if(!isNaN(tempOrigInput))
    {
        origInput = tempOrigInput;
    }
    if(pax1 > minPax && room_count < pax1)
    {
        // let room_diff = Math.abs(paxes[0] - minPax) + room_count;
        sum_room = 1 + (pax1 - minPax);
        $("#input_rooms").val(sum_room);        
    }

    let finalSum = Math.max(sum_room, origInput);
    $("#input_rooms").val(finalSum);

    rooms = finalSum;
    
    updateHotelName();
}

function updatePax()
{
    let paxes = new Array(3);
    let adults = 1;
    if($("#input_adults").val().length > 0)
        adults = parseInt($("#input_adults").val());
    let children = 0;
    if($("#input_children").val().length > 0)
        children = parseInt($("#input_children").val());
    let infant = 0;
    if($("#input_infant").val().length > 0)
        infant = parseInt($("#input_infant").val());

    let pax_total = adults + children + infant;
    $(".pax").each(function()
    {
        $(this).html(pax_total + ' pax');
        let parent = $(this).parent();
        let newSubtotal = pax_total * parseFloat(parent.attr('data-price'));
        parent.find('.subtotals').text(getFormatedMoney(newSubtotal));
        parent.find('.subtotals').attr('data-value', newSubtotal);
    });

    paxes[0] = adults;
    paxes[1] = children;
    paxes[2] = infant;

    if(minPax > -1)
    {
        let allPax = paxes[0] + paxes[1] + paxes[2];
        if(allPax < minPax)
        {
            if(paxes[0] > 0)
            {
                paxes[0] += minPax - allPax;
            }
            else
            {
                if(paxes[1] > minPax || paxes[2] > minPax)
                    paxes[0] = (paxes[1] + paxes[2]) - minPax;
                else
                    paxes[0] = Math.min(minPax, minPax - (paxes[1] + paxes[2]));                
            }
        }

        if((paxes[1] >= minPax || paxes[2] >= minPax) && paxes[0] <= 0)
            paxes[0] = 1;
    }

    $("#input_adults").val(Math.max(1, paxes[0]));

    updateRoom();
        

    let current_travel_date = $("#travel_date").html();
    $("#travel_date").html(current_travel_date.split("<br>")[0] + '<br> Adult: '+ adults +' Children: ' + children +' Infant: ' + infant
    +'<br>' + 'Preferred Hotel : ' +  $("#preferred_hotel").val());
    //+"<br>Rooms:"+rooms);

    computeSubTotal();

    return paxes;
}

function showHighlight(){
    if(selected_promo != null){
        $('.tour_entry').removeClass("selected-item");
        $('.tour_entry[data-key=' + selected_promo.key + ']')
        .each(function(){
            $(this).addClass("selected-item");
        });
    }
}

function showTab(n) {
  // This function will display the specified tab of the form ...
  var x = document.getElementsByClassName("tab");
  /*if(x.length < 4)
    n--;*/

  x[n].style.display = "block";
  // ... and fix the Previous/Next buttons:
  if (n == 0) {
    document.getElementById("prevBtn").style.display = "none";
  } else {
    //if(n < x.length && booked == false)
        document.getElementById("prevBtn").style.display = "inline";
  }
  if (n == (x.length - 1)) {
    document.getElementById("nextBtn").innerHTML = "Book Now";
  } else {
    document.getElementById("nextBtn").innerHTML = "Next";
  }

  if(currentTab == 1)
    $("#currentTabTitle").html((n + 1)+ ". " + tabTitles[n] + "<br>" + selected_promo.name);
  else
    $("#currentTabTitle").text((n + 1)+ ". " + tabTitles[n]);
  // ... and run a function that displays the correct step indicator:
  fixStepIndicator(n)
}

function nextPrev(n) {
    if(n == 1 && currentTab + 1 >= 3 && emailIsVerified == true){
        goSubmit();
        return false;
    }
  // This function will figure out which tab to display
  var x = document.getElementsByClassName("tab");

  if(n == 1 && currentTab == 0 && selected_promo != undefined && !("name" in selected_promo) ){
    bootbox.alert("Please select a destination!");
    return false;
  }

  if ((n == 1 && !$("#mainForm").get(0).reportValidity() 
    && currentTab == 1)) {
        bootbox.alert("Please complete the form!");
        
    return false;
  }

  if(currentTab == 1){
    $("#emailVerificationText").text("Email verification was sent to : " + $("#input_email").val());
    if(emailIsVerified == false && emailSent == false)
        verifyMyEmail();
    else{
        /*x[currentTab].style.display = "none";
        currentTab = currentTab + n;
        if (currentTab >= x.length) {
            document.getElementById("regForm").submit();
            return false;
        }*/
    }
  }    

  if(n == 1 && currentTab == 2 && emailIsVerified == false){
    bootbox.alert("Please verify your email first!");
    
    return false;
  }

  // Hide the current tab:
  x[currentTab].style.display = "none";
  // Increase or decrease the current tab by 1:
  currentTab = currentTab + n;
  // if you have reached the end of the form... :
  if (currentTab >= x.length) {
    //...the form gets submitted:
    goSubmit();
    //document.getElementById("regForm").submit();
    return false;
  }
  // Otherwise, display the correct tab:
  showTab(currentTab);
}

function fixStepIndicator(n) {
  // This function removes the "active" class of all steps...
  var i, x = document.getElementsByClassName("step");
  for (i = 0; i < x.length; i++) {
    x[i].className = x[i].className.replace(" active", "");
  }
  //... and adds the "active" class to the current step:
  x[n].className += " active";
}

function verifyMyEmail(){

    let email = $("#input_email").val();

    let paramString = 'fName=' + $("#input_first_name").val()
                    + '&' + 'lName=' + $("#input_last_name").val()
                    + '&' + 'email=' + $("#input_email").val()
                    + '&' + 'phoneNumber=' + $("#input_phone").val()
                    + '&' + 'address=' + $("#input_address").val()
                    + '&' + 'nationality=' + $("#input_nationality").val().toLowerCase()
                    + '&' + 'travelDateFrom=' + moment($("#input_travel_date_from").val(), "MM/DD/YYYY").format('YYYY-MM-DD')
                    + '&' + 'travelDateTo=' + moment($("#input_travel_date_to").val(), "MM/DD/YYYY").format('YYYY-MM-DD')
                    + '&' + 'adults=' + $("#input_adults").val()
                    + '&' + 'children=' + $("#input_children").val()
                    + '&' + 'infant=' + $("#input_infant").val()
                    + '&' + 'selectedPromoKey=' + selected_promo.key;


    var actionCodeSettings = {
      // URL you want to redirect back to. The domain (www.example.com) for this
      // URL must be in the authorized domains list in the Firebase Console.
      url: 'https://webook-c003e.web.app/create.html?' + paramString,
      // This must be true.
      handleCodeInApp: true,
      /*iOS: {
        bundleId: 'com.example.ios'
      },
      android: {
        packageName: 'com.example.android',
        installApp: true,
        minimumVersion: '12'
      },*/
      //dynamicLinkDomain: 'https://webook-c003e.web.app'
    };

    firebase.auth().sendSignInLinkToEmail(email, actionCodeSettings)
      .then(() => {
        // The link was successfully sent. Inform the user.
        // Save the email locally so you don't need to ask the user for it again
        // if they open the link on the same device.
        window.localStorage.setItem('emailForSignIn', email);

        emailSent = true;
        //window.localStorage.setItem('selected_promo', selected_promo.key);
        // ...
      })
      .catch((error) => {
        var errorCode = error.code;
        var errorMessage = error.message;

        bootbox.alert(error.message);
        // ...
      });
}

function fillFormFromUrl(){
    var url_string = window.location.href;
    var url = new URL(url_string);
    var code = url.searchParams.get("oobCode");

    if(code == null)
        return;
    
    let selectedPromoKey = url.searchParams.get("selectedPromoKey");
    console.log("selectedPromoKey=" + selectedPromoKey);
    if(selectedPromoKey in cached_tours)
        selected_promo = cached_tours[selectedPromoKey];

    let fName = url.searchParams.get("fName");
    let lName = url.searchParams.get("lName");
    let email = url.searchParams.get("email");
    let phoneNumber = url.searchParams.get("phoneNumber");
    let address = url.searchParams.get("address");
    let nationality = url.searchParams.get("nationality");
    let travelDateFrom = url.searchParams.get("travelDateFrom");
    let travelDateTo = url.searchParams.get("travelDateTo");
    let adults = url.searchParams.get("adults");
    let children = url.searchParams.get("children");
    let infant = url.searchParams.get("infant");

    $("#input_first_name").val((fName != null)? fName : "");
    $("#input_last_name").val((lName != null)? lName : "");
    $("#input_email").val((email != null)? email : "");
    $("#input_phone").val((phoneNumber != null)? phoneNumber : "");
    $("#input_address").val((address != null)? address : "");
    $("#input_nationality").val((nationality != null)? nationality : "");
    
    $("#input_adults").val((adults != null)? adults : "");
    $("#input_children").val((children != null)? children : "");
    $("#input_infant").val((infant != null)? infant : "");

    let now = moment();
    let today = now.toDate();

    let endPromoDate = now.add(3, 'days').toDate();

    if(('days' in selected_promo))
        endPromoDate = now.add(selected_promo.days, 'days').toDate();

    let dateFrom = moment(travelDateFrom, "YYYY-MM-DD");
    $('#input_travel_date_from').datepicker("setDate", (dateFrom.isValid())? dateFrom.toDate():  today);
    
    let dateTo = moment(travelDateTo, "YYYY-MM-DD");
    $('#input_travel_date_to').datepicker("setDate", (dateTo.isValid())? dateTo.toDate() : endPromoDate);

    

    updateAllInfos();


}

function checkIfVerifyWithLink(){
    // Confirm the link is a sign-in with email link.
    if (firebase.auth().isSignInWithEmailLink(window.location.href)) {
      var email = window.localStorage.getItem('emailForSignIn');
      if (email == null || email == undefined) {
        email = window.prompt('Please provide your email for confirmation');
      }

      firebase.auth().signInWithEmailLink(email, window.location.href)
        .then((result) => {
            window.localStorage.removeItem('emailForSignIn');

            fillFormFromUrl();

            emailIsVerified = true;
            document.getElementsByClassName("tab")[0].style.display = "none";

            tabTitles.splice(2, 1);

            $(document.getElementsByClassName("tab")[2]).remove();
            currentTab = 2;

            showTab(currentTab);

            
        })
        .catch((error) => {
            bootbox.alert(error.message);            
        });
    }
    else
    {
        showTab(currentTab);
    }
}

function fillTestForm(){

    $("#input_first_name").val("Amiel");
    $("#input_last_name").val("Danao");
    $("#input_email").val("amiel.tbpo@gmail.com");
    $("#input_phone").val("09954261220");
    $("#input_address").val("Pasig");
    $("#input_nationality").val("filipino");
    
    let now = moment();
    let today = now.toDate();

    let endPromoDate = now.add(3, 'days').toDate();
    $('#input_travel_date_from').datepicker("setDate", today );
    
    $('#input_travel_date_to').datepicker("setDate", endPromoDate );

    $("#input_adults").val(1);
    $("#input_children").val("");
    $("#input_infant").val("");
}


function updateAllInfos(){
    nationalityChanged();
    updateAddress();
    updatePhone();
    emailChanged();
    nameChanged();
}