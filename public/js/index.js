$(document).ready(function(){
    $("#local_select").on('click', function(){
        sessionStorage.setItem('goto', 'local');
        window.location = "tours.html";
    });

    $("#international_select").on('click', function(){
        sessionStorage.setItem('goto', 'international');
        window.location = "tours.html";
    });

    attachButtonCallbacks();
    $(document).click();

    // Get media - with autoplay disabled (audio or video)
    var media = $('video').not("[autoplay='autoplay']");
    media.get(0).play();
    var tolerancePixel = 40;

    function checkMedia(){
        // Get current browser top and bottom
        var scrollTop = $(window).scrollTop() + tolerancePixel;
        var scrollBottom = $(window).scrollTop() + $(window).height() - tolerancePixel;

        media.each(function(index, el) {
            

            var yTopMedia = $(this).offset().top;
            var yBottomMedia = $(this).height() + yTopMedia;

            if(scrollTop < yBottomMedia && scrollBottom > yTopMedia){ //view explaination in `In brief` section above
                $(this).get(0).play();
            } else {
                $(this).get(0).pause();
            }
        });

        //}
    }

    $(document).on('scroll', checkMedia);
    $(window).on('wheel', checkMedia);
    


});


$(window).resize(function(){
	let newSize = '10vh';
	if(screen.width < screen.height)
		newSize = '10vw';
	$('.p_destination').css('font-size', newSize);
	console.log('resized');
});

var searching_dialog;

function getResultEntryTemplate(key, img_url, name, price, days, nights)
{
    return `<div onclick="clickedSearchEntry(this);" data-key="`+key+`" class="result_entry col-4"><img class="img-fluid result_entry_image" src="`+img_url+`"/> 
    <div class="caption">
        <h4 class="tour_title">`+name+`</h4>
        <p class="tour_infos">`+getFormatedMoney(price)+`<br>`+days+` Day(s) &amp; `+nights+` Night(s)</p>    
    </div>
    </div>`;
}

function clickedSearchEntry(elem)
{
    let key = $(elem).attr('data-key');

    sessionStorage.setItem('goto', '');

    sessionStorage.setItem("searched_key", key);
    window.location.href = "tours.html";
}


function search(searchName)
{
    var searchIDResults = [];
    
    searching_dialog = bootbox.dialog({
        title: '<p class="text-center mb-0"><i class="fa fa-spin fa-cog"></i> Searching...</p>',
        message:`<div id="result_container" class="row"> </div>`,
        closeButton: false,
        size: "large",
        centerVertical: true,
        buttons: {
            cancel: {
                label: 'Close',
                className: 'btn-danger'
            }
        },
    });

    searchName = searchName.toLowerCase();
    var tours = db.collection("Destinations");
    tours.orderBy("country").startAt(searchName).endAt(searchName + "\uf8ff")
    .get().then(function (querySnapshot) {
        
        querySnapshot.forEach(function (doc) {
            // console.log(doc.id, ' => ', doc.data());
            if(!searchIDResults.includes(doc.id)){
                searchIDResults.push(doc.id);
                // $("#result_container").append(getResultEntryTemplate('images/photos/europe2.jpg'));
            }
                
        });

        var tours2 = db.collection("Destinations");
        tours2.orderBy("region").startAt(searchName).endAt(searchName + "\uf8ff")
        .get().then(function (querySnapshot2){
            querySnapshot2.forEach(function (doc2) {
                // console.log(doc2.id, ' => ', doc2.data());
                if(!searchIDResults.includes(doc2.id))
                {
                    searchIDResults.push(doc2.id);
                    
                }
                    
            });

            for (let index = 0; index < searchIDResults.length; index++) {
                const element = searchIDResults[index];
                
                db.collection("Destinations").doc(element)
                .collection("tours")
                .get().then(function(querySnapshot3){
                    querySnapshot3.forEach(function(doc3){
                        $("#result_container").append(getResultEntryTemplate(doc3.id, doc3.data().image, doc3.data().name, doc3.data().price, doc3.data().days, doc3.data().nights));

                        // console.log(element);
                    });

                    console.log(element);
                   
                });
            }

            console.log(searchIDResults);

            // do something in the background
            // searching_dialog.modal('hide');
            
        });

        
    });
}

function attachButtonCallbacks()
{
    

    $("#search_button").on('click', function(){
        search($("#search-box").val());
    });
}