var cached_tours = [];
var selected_tour_key;
var goto;
var clicked = false;

$(".tourPreviewShow").hide();

$(document).ready( function () {
    addButtonsCallback();   
    fetchDatabaseTourList(); 
    goto = sessionStorage.getItem('goto');
    
    
});

function processClickCustomButton(elem)
{
    $(".info_tab_item").each(function(){
        $(this).hide();
    });

    $(".custom_buttons").each(function(){
        $(this).find('path').get(0).setAttribute('style', 'fill:#ffffff;fill-opacity:1;fill-rule:evenodd;stroke:#000000;stroke-width:1;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1');
    
    });

    let toColor = $('.to_color');
    for(var i=0; i<toColor.length; i++)
    {
        toColor[i].setAttribute('fill', '#000000');
    }

    let toColorText = $('.to_colorText');
    for(var k=0; k<toColorText.length; k++)
    {
        toColorText[k].setAttribute('style', 'fill:#000000;');
    }

    elem.find('path').get(0).setAttribute('style', 'fill:#ffffff;fill-opacity:1;fill-rule:evenodd;stroke:#000000;stroke-width:1;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:0');
   
    let myIcons = elem.find('.to_color');
    for(var l=0; l<myIcons.length; l++){
        myIcons[l].setAttribute('fill', '#00ffff');
    }

    let myTexts = elem.find('.to_colorText');
    for(var m=0; m<myTexts.length; m++){
        myTexts[m].setAttribute('style', 'fill:#00ffff;');
    }
    
    
    let selected_tour_data = cached_tours[selected_tour_key];
    //toggle information
    if(elem.get(0).id == "custom_button1")
    {
        $("#info_table").show();
    }
    if(elem.get(0).id == "custom_button2")
    {
        $("#tour_plan").show();
    }
    if(elem.get(0).id == "custom_button3")
    {
        $("#tour_details").show();
    }
    if(elem.get(0).id == "custom_button4")
    {
        $("#tour_brochure").show();
    }
}

function fillInfoValues()
{
    let selected_tour_data = cached_tours[selected_tour_key];

    $("#info_table_body").empty();
    //show info tab
    
    var obj = JSON.parse(selected_tour_data.tour_info);
    console.log(obj);

    for (const [key, value] of Object.entries(obj)) {
        // console.log(`${key}: ${value}`);

        let retVal = value;
        let retVal2 = "";
        let createNewColumn = false;

        if(key == "Included" || key == "Not included"){

            let icon = '<span class="red"><i class="fas fa-times"></i></span>';
            let include_id = "not-included";

            if(key == "Included"){
                icon = `<span class="green"><i class="fas fa-check"></i></span>`;
                include_id = "included";
            }

            retVal = 
                `<ul class="list-group" id="`+include_id+`">`;

            let first_count = Math.round(value.length/2);

            
            if(value.length > 5 && createNewColumn == false)
            {
                createNewColumn = true;
                
                retVal2 = 
                `<ul class="list-group" id="`+include_id+`">`;
            }                

            for(var k=0;k<value.length; k++)
            {
                if(k < first_count || createNewColumn == false)
                    retVal +=`<li class="list-group-item">`+icon+` `+obj[key][k]+`</li>`;
                else
                    retVal2 +=`<li class="list-group-item">`+icon+` `+obj[key][k]+`</li>`;
                // $("#info_table").append();
            }

            retVal+=  `</ul>`;
            if(createNewColumn == true)
                retVal2 +=  `</ul>`;
        }
            

        let newRow = `<tr>
            <td class="py-4"><p><b>`+key+`</b></p></td>
            
            <td style="width:50%;" class="py-4 tour_info_value">`+retVal+`</td>
        </tr>`;

        if(key == "Included" || key == "Not included")
        {
            newRow = `<tr>
                <td class="py-4"><p><b>`+key+`</b></p></td>
                <td class="py-4 tour_info_value">`+retVal+`</td>
                <td class="py-4 tour_info_value">`+retVal2+`</td>
            </tr>`;
        }


        $("#info_table_body").append(newRow);
    }


    $("#tour_plan").empty();
    var obj_plan = JSON.parse(selected_tour_data.tour_plan);
    console.log(obj_plan);

    for (const [key, value] of Object.entries(obj_plan)) {
        console.log(`${key}: ${value}`);


        var newPlanList = `<li><p class="stepTitle">`+key+`</p><br><p>`+value+`</p</li>`

        $("#tour_plan").append(newPlanList);
    }

    $("#tour_details").empty();

    $("#tour_details").append(selected_tour_data.tour_details);

    $("#brochure_image").attr('src', selected_tour_data.tour_brochure);
}

function updateTourVisibility(myRegion)
{
    $(".tour_select").each(function(){
        let thisRegion = $(this).attr('data-region');
        let thisCountry = $(this).attr('data-country');
        
        if(thisRegion == myRegion || myRegion == 'all' || (myRegion == "philippines" && thisCountry == "philippines"))
        {
            $(this).css('display', 'block');
        }
        else
        {
            $(this).css('display', 'none');
        }
    });
}

function addButtonsCallback()
{

    $(".filter_region").on('click', function(){ 
        clicked = true;       
        $(".filter_region").removeClass("active");
        $(this).addClass("active");

        let myRegion = $(this).text().toLowerCase();

        updateTourVisibility(myRegion);        
    });

    $(".custom_buttons").on('click', function(){
        //restore other link to default color
        processClickCustomButton($(this));
    });

    lightbox.option({
        alwaysShowNavOnTouchDevices: true,

    });

    $(".filter_region").on('click', function(){        
        $(".filter_region").removeClass("active");
        $(this).addClass("active");

        let myRegion = $(this).text().toLowerCase();
    });

    $("#tour_thumbnails_container").on('click', '.tour_select',function(){
        console.log('clicked');

        selected_tour_key = $(this).attr('data-key');

        clickedPromo();
    });

    // fitty('#local_title');
    // resizeScreen();
    // window.onresize = function(event) {
    //     resizeScreen();
    // };
}

function clickedPromo()
{
    let selected_tour_data = cached_tours[selected_tour_key];

    fillInfoValues();

    hideTourSelections();

    $(".info_tab_item").each(function(){
        $(this).hide();
    });
    $("#info_table").show();
    

    $("#selected_tour_title").text(selected_tour_data.name);
    $("#preview_package_text").text(selected_tour_data.days + 'days ' + selected_tour_data.nights + ' nights package');
    
	let formattedMoney = getFormatedMoney(selected_tour_data.price_per_person);
	if(('currency' in selected_tour_data))
		formattedMoney = getFormatedMoneyWithCurrency(selected_tour_data.price_per_person, selected_tour_data['currency']);
	$("#preview_package_price").html(formattedMoney  + ' per person <br> <small>(minimum of ' + selected_tour_data.min_pax + ' pax)</small>');

    $("#carousel_items_container").empty();
    

    var active_text = 'active';
    for (let index = 0; index < selected_tour_data.images.length; index++) {
        const image = selected_tour_data.images[index];
        

        let newEntry = 
        `<div class="carousel-item `+active_text+`">
            <div class="col-lg-3">
                <a href="`+image+`" data-lightbox="image_preview">
                    <img class="img-fluid" src="`+image+`">
                </a>
            </div>
        </div>`;


        $("#carousel_items_container").append(newEntry);

        if(active_text.length > 0)
            active_text = '';
    }

    initCarousel();
}

function initCarousel()
{
    $('#myCarousel').carousel({
        interval: 10000
      });
      
      var i = 0;
      $('.carousel .carousel-item').each(function(){
          var minPerSlide = 4;
          var next = $(this).next();
          if (!next.length) {
            next = $(this).siblings(':first');
          }
          next.children(':first-child').clone().appendTo($(this));
          
          for (var i=0;i<minPerSlide;i++) {
              next=next.next();
              if (!next.length) {
                  next = $(this).siblings(':first');
                }
              
              let cloned = next.children(':first-child').clone();
              $(cloned).find("a").attr('data-lightbox', 'image_preview' + i);
              cloned.appendTo($(this));
            }

            i++;
      });
}


function hideTourSelections()
{
    $(".tourSelectionHide").fadeOut(500);
    
    $(".tourPreviewShow").fadeIn(500);
    
    $(".navbar-brand > img").attr("src", "images/logo_black.png");
    $("body").css("background-image", "none");
}

function fetchDatabaseTourList()
{
    $("#tour_thumbnails_container").empty();

    var active_text = 'active';
    db.collection("Destinations").get().then(function(querySnapshot) {

    
        querySnapshot.forEach(function(doc) {
            // doc.data() is never undefined for query doc snapshots
            
            let data = doc.data();

            db.collection("Destinations").doc(doc.id).collection("tours")
            .get().then(function(querySnapshot2){
                let requests = querySnapshot2.docs.map((tour) => {
                // querySnapshot2.forEach(function(tour){

                    let tour_data = tour.data();
                    cached_tours[tour.id] = tour_data;
                    let price = parseInt(tour_data['price']);
					let formattedMoney = getFormatedMoney(price);
					if(('currency' in tour_data))
						formattedMoney = getFormatedMoneyWithCurrency(price, tour_data['currency']);
					
                    let newEntry = 
                    `<div class="col-xs-18 col-sm-6 col-md-4 tour_select" data-key="`+tour.id+`" data-region="`+data.region+`" data-country="`+data.country+`">
                        <div class="thumbnail">
                        <img src="`+ tour_data['image'] +`" alt="">
                            <div class="caption">
                            <h4 class="tour_title">`+tour_data['name']+`</h4>
                            <p class="tour_infos">`+formattedMoney+`<br>`+tour_data['days']+` Days & `+tour_data['nights']+` Nights</p>
                            
                        </div>
                        </div>
                    </div>`;

                    $("#tour_thumbnails_container").append(newEntry);

                    if(active_text.length > 0)
                        active_text = '';

                    if(goto != null && clicked == false)
                    {
                        let goto_region = "local";
                        if(goto == "local")
                            $("#local_filter_region").trigger('click');
                        if(goto == "international"){
                            $("#international_filter_region").trigger('click');
                            goto_region = "europe";
                        }

                        setTimeout(function(){updateTourVisibility(goto_region);}, 1000);
                        

                        goto = null;
                    }

                    return new Promise((resolve) => {
                        asyncFunction(tour, resolve);
                    });

                });

                Promise.all(requests).then(() => {
                
                    
                    let searched_key = sessionStorage.getItem('searched_key');
                    if(searched_key != undefined && searched_key.length > 0)
                    {
                        selected_tour_key = searched_key;
                        clickedPromo();
                        sessionStorage.setItem('searched_key', '');
                    }
                
                    }, function(error) {
                });
            });
        });        
    });

}


function asyncFunction(item, cb) {
    setTimeout(() => {

        cb();
    }, 100);
}
